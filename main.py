import json 
from urllib.parse import quote_plus
import requests
import yaml
from dotenv import dotenv_values

env = dotenv_values(".env")
blocks = []
blocks.append(
    {
        "type": "header",
        "text": {
            "type": "plain_text",
            "text": "Récapitulatif de la disponibilité sur les 30 derniers jours : "
        }
    }
)
with open('instances.yml') as f:
    categories = yaml.load(f, Loader=yaml.FullLoader)
    for categoryName, categoryContent in categories.items():
        blocks.append(
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": categoryName
                }
	    	}
        )
        print(categoryName)
        for (nom, url) in categoryContent.items():
            q = "sum_over_time(probe_success{instance=\"" + url + "\", job=\"blackbox\"}[30d]) * 100 / (30*24*60*4)"
            q = env['PROMETHEUS_API_ENDPOINT'] + "query?query=" + quote_plus(q, safe='')
            r = requests.get(q)
            score = float(r.json()["data"]["result"][0]["value"][1])
            print(f"\t{nom}, {url}, " + '{0:3.1f}'.format(float(score)))
            if score > 99.5:
                circle = "large_green_circle"
            elif score > 99:
                circle = "large_yellow_circle"
            else:
                circle = "red_circle"
            blocks.append(
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f":{circle}: <{url}|{nom}> - " + '{0:4.2f}'.format(score) + "%\n"
                    }
		        }
            )

data = {
    "channel": env['SLACK_CHANNEL'],
    "blocks": blocks
}

r = requests.post(env['SLACK_WEBHOOK'], data=json.dumps(data))
